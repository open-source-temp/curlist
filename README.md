<h1><a id="Currency_list__Laravel_57_0"></a>Currency list - Laravel 5.7</h1>
<h3><a id="___Laravel_______1"></a>Laravel на примере вывода списка курсов валют</h3>
<p>Приложение для вывода курсов валют. Используется bootstrap-4, внутри найдете пример вывода по средством шаблонизатора blade и посредствои vue.js используя компонентный подход.</p>
<p>Если хотите улучшить этот довольно простой пример, то смело можете присылать код на ревью.</p>
<h3><a id="Installation_5"></a>Installation</h3>
<p>Чтобы приступить к работе, произведите следующие действия</p>
<ul>
<li>Установите локальный веб-сервер с поддержкой Laravel 5.7;</li>
<li>Установите composer;</li>
<li>Установите git;</li>
<li>Установите node.js, npm;</li>
<li>Выполните инструкции ниже.</li>
</ul>
<pre><code class="language-sh">
$ cd my_projects/
$ git <span class="hljs-built_in">clone</span> https://gitlab.com/open-source-temp/curlist.git
$ composer install
$ npm i
$ npm run dev
</code></pre>
<ul>
<li>Произведите настройку .env используя .env.example</li>
<pre><code class="language-sh">
$ cp .env.example .env
$ php artisan key:generate
</code></pre>
</ul>
<p>Желаю Удачи!</p>
