@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-3">
            <button class="btn btn-info text-white" onclick="window.location.reload()">Обновить</button>
        </div>
        <div class="col-9 text-right">
            Дата: @empty($currencies->as_of) не определено @else {{ $currencies->as_of }} @endempty
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col">
            <table class="table table-striped table-bordered">

                <thead>
                    <tr class="text-white bg-primary">
                        <th scope="col">Название валюты</th>
                        <th scope="col">Цена</th>
                        <th scope="col">Количество</th>
                    </tr>
                </thead>

                <tbody>
                    @if($currencies->isNotEmpty())
                       @foreach($currencies as $currency)
                           <tr>
                               <th>{{ $currency->getName() }}</th>
                               <td>{{ $currency->getVolume() }}</td>
                               <td>{{ $currency->getPriceAmount() }}</td>
                           </tr>
                       @endforeach
                    @else
                        <tr>
                            <td colspan="3" class="text-center text-secondary">Валют не найдено</td>
                        </tr>
                    @endif
                </tbody>

            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        setTimeout("window.location.reload()", 15000);
    </script>
@endsection
