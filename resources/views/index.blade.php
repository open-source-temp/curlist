@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col text-center">
            <h1 class="text-dark mb-4">Тестовое задание: Список валют</h1>
            <a href="{{ route('blade') }}" class="btn btn-primary mx-2">Решение на уровне blade</a>
            <a href="{{ route('vue') }}" class="btn btn-primary mx-2">Решение на уровне vue.js</a>
        </div>
    </div>
@endsection
