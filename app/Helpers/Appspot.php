<?php
/**
 * Created by PhpStorm.
 * User: valentinanokhin
 * Date: 09.10.2018
 * Time: 13:39
 */

namespace App\Helpers;

class Appspot
{
    /** @var string */
    protected $name;

    /** @var \stdClass */
    protected $price;

    /** @var float */
    protected $percent_change;

    /** @var integer */
    protected $volume;

    /** @var string */
    protected $symbol;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $value
     * @return Appspot
     */
    public function setName(string $value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceAmount()
    {
        $amount = $this->price->amount ?? 0;
        return round($amount, 2);
    }

    /**
     * @param \stdClass $value
     * @return Appspot
     */
    public function setPrice(\stdClass $value)
    {
        $this->price = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getPercentChange()
    {
        return $this->percent_change;
    }

    /**
     * @param float $value
     * @return Appspot
     */
    public function setPercentChange(float $value)
    {
        $this->percent_change = $value;
        return $this;
    }

    /**
     * @return integer
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param integer $value
     * @return Appspot
     */
    public function setVolume(int $value)
    {
        $this->volume = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param string $value
     * @return Appspot
     */
    public function setSymbol(string $value)
    {
        $this->symbol = $value;
        return $this;
    }

}
