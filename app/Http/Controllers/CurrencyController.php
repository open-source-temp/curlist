<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Helpers\Appspot;

class CurrencyController extends Controller
{
    /** @var string */
    private $linkApi;

    public function __construct()
    {
        // Is setted link
        if (empty(env('API_LINK')))
            abort(500, 'API_LINK not set');

        $this->linkApi = env('API_LINK');
    }

    /**
     * @return \stdClass
     */
    public function getData()
    {
        // Get data API
        $response = file_get_contents($this->linkApi);

        // Is valid response
        if (empty($response))
            abort(500, $this->linkApi . ' - return empty response');

        // Decode response
        /** @var \stdClass $currencies */
        $currencies = json_decode($response);

        // Is valid currency list
        if (empty($currencies->stock) || ! is_array($currencies->stock))
            abort(500, 'Api response does not contain the necessary data');

        return $currencies;
    }

    /**
     * @param \stdClass $currenciesData
     * @return Collection
     */
    public function initCollection(\stdClass $currenciesData)
    {
        $currencies = new Collection();

        $currencies->as_of = $currenciesData->as_of ?? null;

        foreach ($currenciesData->stock as $item)
        {
            // Init currency
            $currency = new Appspot();
            $currency->setName($item->name);
            $currency->setVolume($item->volume);
            $currency->setPrice($item->price);
            $currency->setPercentChange($item->percent_change);
            $currency->setSymbol($item->symbol);

            $currencies->add($currency);
        }

        return $currencies;
    }

    /**
     * @return Collection
     */
    public function getCollectionData()
    {
        $currencies = $this->getData();
        return $this->initCollection($currencies);
    }
}
