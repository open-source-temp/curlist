<?php

namespace App\Http\Controllers;

use App\Helpers\Appspot;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function blade()
    {
        // Init currencies
        $CurrencyController = new CurrencyController();

        /** @var Collection $currencies */
        $currencies = $CurrencyController->getCollectionData();

        return view('blade.index', compact('currencies'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function vue()
    {
        return view('vue.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function json()
    {
        // Init currencies
        $CurrencyController = new CurrencyController();

        /** @var Collection $currencies */
        $currencies = $CurrencyController->getCollectionData();

        $result['as_of'] = $currencies->as_of ?? 'Не определено';

        // Compact currencies
        /** @var Appspot $currency */
        foreach ($currencies as $currency) {
            $result['currencies'][] = [
                'name' => $currency->getName(),
                'volume' => $currency->getVolume(),
                'amount' => $currency->getPriceAmount()
            ];
        }

        return response()->json($result);
    }
}
